# The Paid Desktop (not-so-real) Foundation

Imagine a world. People in this world do not pay for software with their money or data. They pay for this software with their precious time. We, at the Paid Desktop (not-so-real) Foundation aim to reach that world. 

# Wait, isn't that just Free Software? Or open source software?

We do not particularly use FOSS because we believe FOSS is ethical. We use FOSS because paying for software with your money is unethical. You should pay with your time instead!

# So, you want us to waste time to learn your software?

We wouldn't call that "wastage" of time. Sometimes you can learn something special from our software!

# Well, I am not gonna use your software if I have to spend more time learning it that spending time to get my things done.

The Paid Desktop (not-so-real) Foundation does not want people to forcibly use their software - we cater to the users who want to learn how to use their software and pay us with their time and expertise.